# 蓝色兄弟
使用前请务必详细阅读本文档，本文档包含
1. 发起请求示例
2. 主动查询充值单信息示例
3. 通知验签示例
4. 解密卡密示例

## 安装
### 1. 通过引入jar包 导入方式安装
* [下载jar包](https://gitee.com/chengdu_blue_brothers/openapi-java-sdk/releases)
* 自行安装依赖项：com.fasterxml.jackson.core

## 发起请求示例
```java
import com.eeext.openapi.constans.RespCode;
import com.eeext.openapi.constans.RespStatus;
import com.eeext.openapi.entity.*;
import java.util.Map;
import java.util.TreeMap;

// 第一步：初使化client实例
String merchantId = "您的商户ID";
String secretKey = "您的商户密钥";
// 是否为生产环境，false表示沙箱环境，true表示生产环境
Boolean isProd = false;
Client client = new Client(merchantId, secretKey, isProd);

// 第二步：调用相关API接口：以下单为充值示例
RechargeOrderReq req = new RechargeOrderReq();
req.setOutTradeNo("41858615686074369");
req.setProductId(101);
req.setRechargeAccount("18012345678");
req.setAccountType(0);
req.setNumber(1);
req.setNotifyUrl("http://回调地址");
RechargeOrderResp resp = client.rechargeOrder(req);
// 第三步：判断CODE是否为下单成功。注意：有些接口需要判断api.CodeResponseSuccess，具体以接口详情为准
if (resp.isSuccess()) {
    //...下单成功并不表示充值成功，需要在回调通知中判断订单状态
}
```

## 订单通知结果验签与解密卡密示例
```java
import com.eeext.openapi.constans.RespCode;
import com.eeext.openapi.constans.RespStatus;
import com.eeext.openapi.entity.*;
import java.util.Map;
import java.util.TreeMap;

// 第一步：初使化client实例
String merchantId = "您的商户ID";
String secretKey = "您的商户密钥";
Notify notify = new Notify(merchantId, secretKey);

// 第二步：验签并解析结果
//获取到http的请求参数，拼装为TreeMap<String, String>;
//省略拼装为TreeMap<String, String> postData的过程
try{
    NotifyOrderReq notifyOrderReq=notify.parseAndVerify(postData);
    if(notifyOrderReq.isSuccess()){
        //...充值成功状态
        // 如果是卡密订单且未传mobile，可进一步解密卡密
        Map<String, String> cardMap = DecodeCard.decode2Map(notifyOrderReq.getCardCode(), testSecretKey);
    }else if(notifyOrderReq.isFailed()){
        //...充值失败状态
    }
} catch (Exception e) {
    // 处理异常
}

```