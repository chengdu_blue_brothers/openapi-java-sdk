package com.eeext.openapi;

import com.eeext.openapi.constans.Api;
import com.eeext.openapi.constans.Domain;
import com.eeext.openapi.entity.*;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Map;
import java.util.TreeMap;


/**
 * com.eeext.openapi
 * @author xdg
 */
public class Client {
    /**
     * 商户id
     */
    String merchantId;

    /**
     * 密钥key
     */
    String secretKey;

    /**
     * 是否是生产环境
     */
    Boolean isProd;

    /**
     * 超时时间，单位秒数，默认为30秒
     */
    Integer timeout = 30;

    /**
     * 设置请求超时时间
     * @param timeout 秒
     * @return
     */
    public Client setTimeout(Integer timeout) {
        this.timeout = timeout;
        return this;
    }

    /**
     * 构造方法
     * @param merchantId 商户id
     * @param secretKey 密钥key
     * @param isProd 是否是生产环境
     */
    public Client(String merchantId, String secretKey, Boolean isProd) {
        this.merchantId = merchantId;
        this.secretKey = secretKey;
        this.isProd = isProd;
    }

    /**
     * 卡密重发短信接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/JkSqwfASnirmjXkQiULc3Qt6nFH
     * @param outTradeNo
     * @throws Exception
     */
    public CardIssueResp cardIssue(String outTradeNo) throws Exception {
        TreeMap<String, String> postData = new TreeMap<>();
        postData.put("outTradeNo", outTradeNo);
        String respBody = request(Api.CARD_ISSUE, postData);
        return parseJson(respBody, CardIssueResp.class);
    }

    /**
     * 卡密下单接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/Xe2owT9nFicNCdkmLJxcjarHnRh
     * @param req
     * @return
     * @throws Exception
     */
    public CardOrderResp cardOrder(CardOrderReq req) throws Exception {
        TreeMap<String, String> postData = new TreeMap<>();
        postData.put("outTradeNo", req.getOutTradeNo());
        postData.put("productId", req.getProductId().toString());
        if (req.getMobile() != null && !req.getMobile().isEmpty()) {
            postData.put("mobile", req.getMobile());
        }
//        postData.put("accountType", req.getAccountType().toString());
        postData.put("number", req.getNumber().toString());
        postData.put("notifyUrl", req.getNotifyUrl());
        if (req.getExtendParameter() != null && !req.getExtendParameter().isEmpty()) {
            postData.put("extendParameter", req.getExtendParameter());
        }
        String respBody = request(Api.CARD_ORDER, postData);
        return parseJson(respBody, CardOrderResp.class);
    }

    /**
     * 卡密订单查询接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/IU7twzg3Di9VRqknompctHuEnmc
     * @param outTradeNo
     * @return
     * @throws Exception
     */
    public CardQueryResp cardQuery(String outTradeNo) throws Exception {
        TreeMap<String, String> postData = new TreeMap<>();
        postData.put("outTradeNo", outTradeNo);
        String respBody = request(Api.CARD_QUERY, postData);
        return parseJson(respBody, CardQueryResp.class);
    }

    /**
     * 余额查询接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/ModNwquZWizj75kaz4ncksxNnqb
     * @return
     */
    public RechargeInfoResp rechargeInfo() throws Exception {
        TreeMap<String, String> postData = new TreeMap<>();
        String respBody = request(Api.RECHARGE_INFO, postData);
        return parseJson(respBody, RechargeInfoResp.class);
    }

    /**
     * 直充下单接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/Kuj7wBHCWiT0SDkY6dMcXuuYn8f
     * @return
     */
    public RechargeOrderResp rechargeOrder(RechargeOrderReq req) throws Exception {
        TreeMap<String, String> postData = new TreeMap<>();
        postData.put("outTradeNo", req.getOutTradeNo());
        postData.put("productId", req.getProductId().toString());
        postData.put("rechargeAccount", req.getRechargeAccount());
        postData.put("accountType", req.getAccountType().toString());
        postData.put("number", req.getNumber().toString());
        postData.put("notifyUrl", req.getNotifyUrl());
        if (req.getExtendParameter() != null && !req.getExtendParameter().isEmpty()) {
            postData.put("extendParameter", req.getExtendParameter());
        }
        String respBody = request(Api.RECHARGE_ORDER, postData);
        return parseJson(respBody, RechargeOrderResp.class);
    }

    /**
     * 获取商品列表接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/XmAiwMQiZiVRxpkuVAgcz2jRnlb
     * @return
     * @throws Exception
     */
    public RechargeProductResp rechargeProduct() throws Exception {
        TreeMap<String, String> postData = new TreeMap<>();
        String respBody = request(Api.RECHARGE_PRODUCT, postData);
        return parseJson(respBody, RechargeProductResp.class);
    }

    /**
     * 直充订单查询接口
     * 接口说明：https://tvd8jq9lqkp.feishu.cn/wiki/PT46wpD0wiH3QvkYWU8csUtlncb
     * @return
     * @throws Exception
     */
    public RechargeQueryResp rechargeQuery(String outTradeNo) throws Exception {
        TreeMap<String, String> postData = new TreeMap<>();
        postData.put("outTradeNo", outTradeNo);
        String respBody = request(Api.RECHARGE_QUERY, postData);
        return parseJson(respBody, RechargeQueryResp.class);
    }

    /**
     * 解析json字符串为对象
     * @param json
     * @param clazz
     * @param <T>
     * @return
     * @throws Exception
     */
    private <T> T parseJson(String json, Class<T> clazz) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(json, clazz);
    }

    /**
     * 发起请求
     * @param path
     * @param postData
     * @return
     * @throws Exception
     */
    private String request(String path, TreeMap<String, String> postData) throws Exception {
        //添加公共参数
        postData.put("merchantId", merchantId);
        postData.put("timeStamp", String.valueOf(System.currentTimeMillis()/1000));
        postData.put("sign", getSign(postData, secretKey));

        // 构建请求的requestBody，值需要做url encode
        StringBuilder requestBody = new StringBuilder();
        for (Map.Entry<String, String> entry : postData.entrySet()) {
            if (requestBody.length() > 0) {
                requestBody.append("&");
            }
            String encodedValue = URLEncoder.encode(entry.getValue(), "UTF-8");
            requestBody.append(entry.getKey()).append("=").append(encodedValue);
        }

        String domain = isProd ? Domain.PROD : Domain.SANDBOX;
        // 发起请求
        URL url = new URL(domain + "/" + path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setReadTimeout(timeout * 1000);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setDoOutput(true);
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(requestBody.toString().getBytes());
        outputStream.flush();
        outputStream.close();

        // 读取响应
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
        StringBuilder response = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            response.append(line);
        }
        reader.close();
        String respBody = response.toString();

        if (!isProd) {
            System.out.printf("请求地址：%s，\n\t响应数据：%s\n", url, respBody);
        }

        return respBody;
    }

    /**
     * 获取sign
     * @param data
     * @param secretKey
     * @return
     * @throws Exception
     */
    public static String getSign(TreeMap<String, String> data, String secretKey) throws Exception {
        StringBuilder rawStr = new StringBuilder();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            if (entry.getKey() == "extendParameter" || entry.getKey() == "sign") {
                //这两个特殊的参数不参与签名
                continue;
            }
            if (rawStr.length() > 0) {
                rawStr.append("&");
            }
            rawStr.append(entry.getKey()).append("=").append(entry.getValue());
        }

        // 第三步：生成签名
        rawStr.append("&key=").append(secretKey);
        String sign = calculateMD5(rawStr.toString()).toUpperCase();
        return sign;
    }

    /**
     * 计算md5
     * @param input
     * @return
     * @throws Exception
     */
    private static String calculateMD5(String input) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashBytes = md.digest(input.getBytes("UTF-8"));

        StringBuilder hexString = new StringBuilder();
        for (byte b : hashBytes) {
            String hex = Integer.toHexString(0xFF & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
