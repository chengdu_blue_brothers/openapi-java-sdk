package com.eeext.openapi;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * com.eeext.openapi
 * @author xdg
 */
public class DecodeCard {
    /**
     * 解密卡密
     * @param encryptedData
     * @param secretKey
     * @return
     * @throws OpenApiException
     */
    public static Map<String, String> decode2Map(String encryptedData, String secretKey) throws Exception {
        String codes = decode2String(encryptedData, secretKey);
        HashMap<String, String> cards = new HashMap<>(2);
        String[] rows = codes.split(",");
        for (String row : rows) {
            String[] card = row.split(":");
            if (card.length == 2) {
                cards.put(card[0], card[1]);
            } else {
                cards.put(row, "");
            }
        }
        return cards;
    }

    /**
     * 解密卡密
     * @param encryptedData
     * @param secretKey
     * @return
     * @throws OpenApiException
     */
    public static String decode2String(String encryptedData, String secretKey) throws Exception {
        // 第一步：对加密的卡密做base64 decode
        byte[] encryptedBytes = Base64.getDecoder().decode(encryptedData);

        // 第二步：使用aes-256-ecb解密
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
        String codes = new String(decryptedBytes, StandardCharsets.UTF_8);
        codes = codes.trim();
        return codes;
    }
}
