package com.eeext.openapi;


import com.eeext.openapi.entity.NotifyOrderReq;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * 通知
 * @author xdg
 */
public class Notify {
    /**
     * 商户id
     */
    private String merchantId;

    /**
     * 密钥key
     */
    private String secretKey;

    /**
     * 构造方法
     * @param merchantId
     * @param secretKey
     */
    public Notify(String merchantId, String secretKey) {
        this.merchantId = merchantId;
        this.secretKey = secretKey;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    /**
     * 解析并验证订单通知
     * @param formData
     * @return
     * @throws Exception
     */
    public NotifyOrderReq parseAndVerify(TreeMap<String, String> formData) throws Exception {
        String sign = Client.getSign(formData, getSecretKey());
        if (!sign.equals(formData.get("sign"))) {
            throw OpenApiException.SIGN_ERROR;
        }

        NotifyOrderReq notifyOrderReq = new NotifyOrderReq();
        notifyOrderReq.setMerchantId(formData.get("merchantId"));
        notifyOrderReq.setOutTradeNo(formData.get("outTradeNo"));
        notifyOrderReq.setStatus(formData.get("status"));
        notifyOrderReq.setRechargeAccount(formData.get("rechargeAccount"));
        notifyOrderReq.setCardCode(formData.get("cardCode"));
        return notifyOrderReq;
    }

    /**
     * 解析卡密
     * @param secretKey
     * @param cardCode
     * @return
     * @throws Exception
     */
    public Map<String, String> decodeCard(String secretKey, String cardCode) throws Exception {
        if (cardCode == null || cardCode.isEmpty()) {
            return new HashMap<>(0);
        }
        return DecodeCard.decode2Map(cardCode, secretKey);
    }
}
