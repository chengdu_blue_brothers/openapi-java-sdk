package com.eeext.openapi;

/**
 * com.eeext.openapi
 * @author xdg
 */
public class OpenApiException extends Exception {
    /**
     * 错误code
     */
    private Integer code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 签名错误
     */
    final public static OpenApiException SIGN_ERROR = new OpenApiException(10001, "签名错误");

    public Integer getCode() {
        return code;
    }

    public OpenApiException setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public OpenApiException setMessage(String message) {
        this.message = message;
        return this;
    }

    public OpenApiException(Integer code, String msg) {
        this.message = msg;
        this.code = code;
    }
}
