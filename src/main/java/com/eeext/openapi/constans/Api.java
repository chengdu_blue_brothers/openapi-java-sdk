package com.eeext.openapi.constans;

/**
 * 接口常量
 * com.eeext.openapi.api
 * @author xdg
 */
public class Api {
    /**
     * 卡密重发短信接口
     */
    final public static String CARD_ISSUE = "/card/issue";

    /**
     * 卡密订单查询接口
     */
    final public static String CARD_QUERY = "/card/query";

    /**
     * 卡密下单接口
     */
    final public static String CARD_ORDER = "/card/order";

    /**
     * 余额查询接口
     */
    final public static String RECHARGE_INFO = "/recharge/info";

    /**
     * 获取商品列表接口
     */
    final public static String RECHARGE_PRODUCT = "/recharge/product";

    /**
     * 直充订单查询接口
     */
    final public static String RECHARGE_QUERY = "/recharge/query";

    /**
     * 直充下单接口
     */
    final public static String RECHARGE_ORDER = "/recharge/order";
}
