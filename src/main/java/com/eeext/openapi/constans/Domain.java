package com.eeext.openapi.constans;

/**
 * com.eeext.openapi.constans
 * @author xdg
 */
public class Domain {
    /**
     * 沙箱环境
     */
    final public static String SANDBOX = "http://test.openapi.1688sup.cn";

    /**
     * 生产环境
     */
    final public static String PROD = "https://openapi.1688sup.com";
}
