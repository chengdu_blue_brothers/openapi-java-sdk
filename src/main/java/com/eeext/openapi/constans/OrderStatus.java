package com.eeext.openapi.constans;

/**
 * com.eeext.openapi.constans
 * @author xdg
 */
public class OrderStatus {
    /**
     * 充值成功
     */
    final public static String SUCCESS = "01";

    /**
     * 充值中
     */
    final public static String RECHARGING = "02";

    /**
     * 充值失败
     */
    final public static String FAIL = "03";

    /**
     * 充值异常，建议商户先调用人工处理
     */
    final public static String EXCEPTION = "04";
}
