package com.eeext.openapi.constans;

/**
 * 响应Code常量
 * com.eeext.openapi.constans
 * @author xdg
 */
public class RespCode {
    /**
     * 响应成功成功
     */
    public static final String SUCCESS = "0000";

    /**
     * 下单成功
     */
    public static final String CREATE_ORDER_SUCCESS = "2000";

    /**
     * 下单重复
     */
    public static final String CREATE_ORDER_REPEAT = "1005";
}
