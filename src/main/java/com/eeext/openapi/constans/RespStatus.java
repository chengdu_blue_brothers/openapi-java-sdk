package com.eeext.openapi.constans;

/**
 * 响应Code常量
 * com.eeext.openapi.constans
 * @author xdg
 */
public class RespStatus {
    /**
     * 重发卡密成功
     */
    public static final String CARD_ISSUE_SUCCESS = "01";

    /**
     * 重发下发失败
     */
    public static final String CARD_ISSUE_FAILED = "03";
}
