package com.eeext.openapi.entity;

import com.eeext.openapi.constans.RespCode;
import com.eeext.openapi.constans.RespStatus;

/**
 * com.eeext.openapi.entity
 * @author xdg
 */
public class CardIssueResp {
    private String code;
    private String message;
    private String status;

    public CardIssueResp setCode(String code) {
        this.code = code;
        return this;
    }

    public CardIssueResp setMessage(String message) {
        this.message = message;
        return this;
    }

    public CardIssueResp setStatus(String status) {
        this.status = status;
        return this;
    }

    public CardIssueResp setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
        return this;
    }

    private String outTradeNo;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public Boolean isSuccess() {
        return code.equals(RespCode.SUCCESS);
    }

    public Boolean isFailed() {
        return !isSuccess();
    }

    public Boolean isSendSuccess() {
        return isSuccess() && status.equals(RespStatus.CARD_ISSUE_SUCCESS);
    }

    public Boolean isSendFailed() {
        return !isSendSuccess();
    }
}
