package com.eeext.openapi.entity;

/**
 * com.eeext.openapi.entity
 * @author xdg
 */
public class CardOrderReq {
    /**
     * 合作商系统内部订单号,同一商户下不可重复， 同微信、支付宝的out_trade_no类似
     */
    private String outTradeNo;

    /**
     * 商品编码
     */
    private Integer productId;

    /**
     * 手机号，非必传
     */
    private String mobile;

    /**
     * @deprecated 未启用
     * 充值账号类型,1：手机号，2：QQ号，0：其他
     */
    private Integer accountType;

    /**
     * 扩展参数
     */
    private String extendParameter = "";

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public Integer getProductId() {
        return productId;
    }

    public String getMobile() {
        return mobile;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public Integer getNumber() {
        return number;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public String getExtendParameter() {
        return extendParameter;
    }

    /**
     * 购买数量，目前只能固定是1
     */
    Integer number;

    /**
     * 异步通知地址
     */
    String notifyUrl;

    public CardOrderReq setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
        return this;
    }

    public CardOrderReq setProductId(Integer productId) {
        this.productId = productId;
        return this;
    }

    public CardOrderReq setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public CardOrderReq setAccountType(Integer accountType) {
        this.accountType = accountType;
        return this;
    }

    public CardOrderReq setNumber(Integer number) {
        this.number = number;
        return this;
    }

    public CardOrderReq setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
        return this;
    }

    public CardOrderReq setExtendParameter(String extendParameter) {
        this.extendParameter = extendParameter;
        return this;
    }
}
