package com.eeext.openapi.entity;

import com.eeext.openapi.DecodeCard;
import com.eeext.openapi.constans.OrderStatus;
import com.eeext.openapi.constans.RespCode;
import java.util.Map;

/**
 * com.eeext.openapi.entity
 * @author xdg
 */
public class CardQueryResp {
    /**
     * 响应码
     */
    String code;

    /**
     * 响应消息
     */
    String message;

    /**
     * 订单状态
     */
    String status;

    /**
     * 合作商系统内部订单号
     */
    String outTradeNo;

    /**
     * 卡号
     */
    String cardCode;

    public String getCode() {
        return code;
    }

    public CardQueryResp setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public CardQueryResp setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public CardQueryResp setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public CardQueryResp setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
        return this;
    }

    public Object getCardCode() {
        return cardCode;
    }

    public CardQueryResp setCardCode(String cardCode) {
        this.cardCode = cardCode;
        return this;
    }

    /**
     * 充值是否成功
     * @return
     */
    public Boolean isSuccess() {
        return RespCode.SUCCESS.equals(code) && OrderStatus.SUCCESS.equals(status);
    }

    /**
     * 充值是否失败
     * @return
     */
    public Boolean isFailed() {
        return OrderStatus.FAIL.equals(status);
    }

    /**
     * 是否充值中
     * @return
     */
    public Boolean isRecharging() {
        return OrderStatus.RECHARGING.equals(status);
    }

    /**
     * 是否异常
     * @return
     */
    public Boolean isException() {
        return OrderStatus.EXCEPTION.equals(status);
    }

    public Map<String, String> decodeCard(String secretKey) throws Exception {
        return DecodeCard.decode2Map(cardCode, secretKey);
    }
}
