package com.eeext.openapi.entity;

import com.eeext.openapi.DecodeCard;
import com.eeext.openapi.constans.OrderStatus;
import com.eeext.openapi.constans.RespCode;

import java.util.Map;

/**
 * com.eeext.openapi.entity
 */
public class NotifyOrderReq {
    /**
     * 商户id
     */
    private String merchantId;

    /**
     * 商户订单号
     */
    private String outTradeNo;

    /**
     * 订单状态
     */
    private String status;

    /**
     * 充值账号
     */
    private String rechargeAccount;

    /**
     * 卡密类型，仅卡密类型订单，且充值成功状态，且创建订单时未传mobile才会存在此项
     */
    private String cardCode;

    /**
     * 签名
     */
    private String sign;

    public String getMerchantId() {
        return merchantId;
    }

    public NotifyOrderReq setMerchantId(String merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public NotifyOrderReq setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public NotifyOrderReq setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getRechargeAccount() {
        return rechargeAccount;
    }

    public NotifyOrderReq setRechargeAccount(String rechargeAccount) {
        this.rechargeAccount = rechargeAccount;
        return this;
    }

    public String getCardCode() {
        return cardCode;
    }

    public NotifyOrderReq setCardCode(String cardCode) {
        this.cardCode = cardCode;
        return this;
    }

    public String getSign() {
        return sign;
    }

    public NotifyOrderReq setSign(String sign) {
        this.sign = sign;
        return this;
    }

    /**
     * 充值是否成功
     * @return
     */
    public Boolean isSuccess() {
        return OrderStatus.SUCCESS.equals(status);
    }

    /**
     * 充值是否失败
     * @return
     */
    public Boolean isFailed() {
        return OrderStatus.FAIL.equals(status);
    }

    /**
     * 是否充值中
     * @return
     */
    public Boolean isRecharging() {
        return OrderStatus.RECHARGING.equals(status);
    }

    /**
     * 是否异常
     * @return
     */
    public Boolean isException() {
        return OrderStatus.EXCEPTION.equals(status);
    }

    public Map<String, String> decodeCard(String secretKey) throws Exception {
        return DecodeCard.decode2Map(cardCode, secretKey);
    }
}
