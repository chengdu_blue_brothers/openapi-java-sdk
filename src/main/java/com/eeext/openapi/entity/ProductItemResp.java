package com.eeext.openapi.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 商品信息
 * @author xdg
 */
public class ProductItemResp {
    /**
     * 商品编码
     */
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 商品原价，单位：元
     */
    @JsonProperty("original_price")
    private Float originalPrice;

    /**
     * 渠道价，单位：元
     */
    @JsonProperty("channel_price")
    private String channelPrice;

    /**
     * 商品名称
     */
    @JsonProperty("item_name")
    private String itemName;

    public Integer getProductId() {
        return productId;
    }

    public ProductItemResp setProductId(Integer productId) {
        this.productId = productId;
        return this;
    }

    public Float getOriginalPrice() {
        return originalPrice;
    }

    public ProductItemResp setOriginalPrice(Float originalPrice) {
        this.originalPrice = originalPrice;
        return this;
    }

    public String getChannelPrice() {
        return channelPrice;
    }

    public ProductItemResp setChannelPrice(String channelPrice) {
        this.channelPrice = channelPrice;
        return this;
    }

    public String getItemName() {
        return itemName;
    }

    public ProductItemResp setItemName(String itemName) {
        this.itemName = itemName;
        return this;
    }
}
