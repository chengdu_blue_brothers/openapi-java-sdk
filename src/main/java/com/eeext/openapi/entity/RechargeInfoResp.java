package com.eeext.openapi.entity;

import com.eeext.openapi.constans.RespCode;

/**
 * com.eeext.openapi.entity
 * @author xdg
 */
public class RechargeInfoResp {
    /**
     * 响应码
     */
    private String code;

    /**
     * 余额，单位：元
     */
    private Float balance;

    public String getCode() {
        return code;
    }

    public RechargeInfoResp setCode(String code) {
        this.code = code;
        return this;
    }

    public Float getBalance() {
        return balance;
    }

    public RechargeInfoResp setBalance(Float balance) {
        this.balance = balance;
        return this;
    }

    /**
     * 请求是否成功
     * @return
     */
    public Boolean isSuccess() {
        return RespCode.SUCCESS.equals(code);
    }

    /**
     * 是否失败
     * @return
     */
    public Boolean isFailed() {
        return !isSuccess();
    }
}
