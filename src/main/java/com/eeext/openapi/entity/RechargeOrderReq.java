package com.eeext.openapi.entity;

/**
 * com.eeext.openapi.entity
 * @author xdg
 */
public class RechargeOrderReq {
    /**
     * 合作商系统内部订单号,同一商户下不可重复， 同微信、支付宝的out_trade_no类似
     */
    private String outTradeNo;

    /**
     * 商品编码
     */
    private Integer productId;

    /**
     * 充值账号
     */
    private String rechargeAccount;

    /**
     * 充值账号类型,1：手机号，2：QQ号，0：其他
     */
    private Integer accountType;

    /**
     * 购买数量，目前只能固定是1
     */
    private Integer number;

    /**
     * 异步通知地址
     */
    private String notifyUrl;

    /**
     * 扩展参数
     */
    private String extendParameter = "";


    public String getOutTradeNo() {
        return outTradeNo;
    }

    public RechargeOrderReq setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
        return this;
    }

    public Integer getProductId() {
        return productId;
    }

    public RechargeOrderReq setProductId(Integer productId) {
        this.productId = productId;
        return this;
    }

    public String getRechargeAccount() {
        return rechargeAccount;
    }

    public RechargeOrderReq setRechargeAccount(String rechargeAccount) {
        this.rechargeAccount = rechargeAccount;
        return this;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public RechargeOrderReq setAccountType(Integer accountType) {
        this.accountType = accountType;
        return this;
    }

    public Integer getNumber() {
        return number;
    }

    public RechargeOrderReq setNumber(Integer number) {
        this.number = number;
        return this;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public RechargeOrderReq setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
        return this;
    }

    public String getExtendParameter() {
        return extendParameter;
    }

    public RechargeOrderReq setExtendParameter(String extendParameter) {
        this.extendParameter = extendParameter;
        return this;
    }
}
