package com.eeext.openapi.entity;

import com.eeext.openapi.constans.RespCode;

/**
 * com.eeext.openapi.entity
 * @author xdg
 */
public class RechargeOrderResp {
    /**
     * 响应的code
     */
    private String code;

    /**
     * 响应的消息
     */
    private String message;

    public String getCode() {
        return code;
    }

    public RechargeOrderResp setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public RechargeOrderResp setMessage(String message) {
        this.message = message;
        return this;
    }

    /**
     * 是否成功
     * @return
     */
    public Boolean isSuccess() {
        return RespCode.CREATE_ORDER_SUCCESS.equals(code);
    }

    /**
     * 是否下单失败
     * @return
     */
    public Boolean isFailed() {
        return !isSuccess();
    }

    /**
     * 是否重复
     * @return
     */
    public Boolean isRepeat() {
        return RespCode.CREATE_ORDER_REPEAT.equals(code);
    }
}
