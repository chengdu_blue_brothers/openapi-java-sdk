package com.eeext.openapi.entity;

import com.eeext.openapi.constans.RespCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品信息
 * @author xdg
 */
public class RechargeProductResp {
    /**
     * 响应的code
     */
    private String code;

    /**
     * 商品列表
     */
    private List<ProductItemResp> products;

    public List<ProductItemResp> getProducts() {
        return products;
    }

    public RechargeProductResp setProducts(List<ProductItemResp> products) {
        this.products = products;
        return this;
    }

    public String getCode() {
        return code;
    }

    public RechargeProductResp setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * 请求是否成功
     * @return
     */
    public Boolean isSuccess() {
        return RespCode.SUCCESS.equals(code);
    }

    /**
     * 是否失败
     * @return
     */
    public Boolean isFailed() {
        return !isSuccess();
    }
}
