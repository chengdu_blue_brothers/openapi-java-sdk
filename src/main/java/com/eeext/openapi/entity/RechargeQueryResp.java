package com.eeext.openapi.entity;

import com.eeext.openapi.DecodeCard;
import com.eeext.openapi.constans.OrderStatus;
import com.eeext.openapi.constans.RespCode;

import java.util.Map;

/**
 * com.eeext.openapi.entity
 * @author xdg
 */
public class RechargeQueryResp {
    /**
     * 响应码
     */
    private String code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 订单状态
     */
    private String status;

    /**
     * 合作商系统内部订单号
     */
    private String outTradeNo;

    public String getCode() {
        return code;
    }

    public RechargeQueryResp setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public RechargeQueryResp setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public RechargeQueryResp setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public RechargeQueryResp setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
        return this;
    }

    /**
     * 充值是否成功
     * @return
     */
    public Boolean isSuccess() {
        return RespCode.SUCCESS.equals(code) && OrderStatus.SUCCESS.equals(status);
    }

    /**
     * 充值是否失败
     * @return
     */
    public Boolean isFailed() {
        return OrderStatus.FAIL.equals(status);
    }

    /**
     * 是否充值中
     * @return
     */
    public Boolean isRecharging() {
        return OrderStatus.RECHARGING.equals(status);
    }

    /**
     * 是否异常
     * @return
     */
    public Boolean isException() {
        return OrderStatus.EXCEPTION.equals(status);
    }
}
