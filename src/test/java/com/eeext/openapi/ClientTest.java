package com.eeext.openapi;

import com.eeext.openapi.constans.RespCode;
import com.eeext.openapi.constans.RespStatus;
import com.eeext.openapi.entity.*;
import junit.framework.TestCase;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.Map;
import java.util.TreeMap;

/**
 * client 的测试
 * com.eeext.openapi
 */
public class ClientTest{
    static String testMerchantId = "23329";
    static String testSecretKey = "8db16e8cc8363ed4eb4c14f9520bcc32";
    static Client client;

    @BeforeClass
    public static void SetClient() {
        ClientTest.client = new Client(testMerchantId, testSecretKey, false);
    }

    @Test
    public void cardIssueTest() throws Exception {
        String outTradeNo = "41858615686074369";
        CardIssueResp resp = client.cardIssue(outTradeNo);
        TestCase.assertEquals(RespCode.SUCCESS, resp.getCode());
        TestCase.assertEquals(RespStatus.CARD_ISSUE_FAILED, resp.getStatus());
        TestCase.assertEquals(outTradeNo, resp.getOutTradeNo());
    }

    @Test
    public void cardOrderTest() throws Exception {
        // 重复
        CardOrderReq req = new CardOrderReq();
        req.setOutTradeNo("41858615686074369");
        req.setProductId(101);
        req.setMobile("18012345678");
//        req.setAccountType(0);
        req.setNumber(1);
        req.setNotifyUrl("http://xxx");
        req.setExtendParameter("extendParameter={\"contactMobile\":\"手机号\"}");
        CardOrderResp resp = client.cardOrder(req);
        TestCase.assertEquals(RespCode.CREATE_ORDER_REPEAT, resp.getCode());

        // 验证成功
        // 单号连接时间戳
        req.setOutTradeNo("41858615686074369_" + System.currentTimeMillis());
        resp = client.cardOrder(req);
        TestCase.assertEquals(RespCode.CREATE_ORDER_SUCCESS, resp.getCode());
    }

    @Test
    public void cardQueryTest() throws Exception {
        String outTradeNo = "17024319210905";
        CardQueryResp resp = client.cardQuery(outTradeNo);
        TestCase.assertEquals(RespCode.SUCCESS, resp.getCode());
        TestCase.assertTrue(resp.isSuccess());
        TestCase.assertEquals(outTradeNo, resp.getOutTradeNo());
        //解码
        Map<String, String> cardMap = resp.decodeCard(testSecretKey);
        TestCase.assertTrue(cardMap.containsKey("5HHC-26EH-E22F-6B861"));
    }

    @Test
    public void rechargeInfoTest() throws Exception {
        RechargeInfoResp resp = client.rechargeInfo();
        TestCase.assertTrue(resp.isSuccess());
        TestCase.assertTrue(resp.getBalance() > 0);
    }

    @Test
    public void rechargeOrder() throws Exception {
        RechargeOrderReq req = new RechargeOrderReq();
        req.setOutTradeNo("41858615686074369");
        req.setProductId(101);
        req.setRechargeAccount("18012345678");
        req.setAccountType(0);
        req.setNumber(1);
        req.setNotifyUrl("http://xxx");
        req.setExtendParameter("extendParameter={\"contactMobile\":\"手机号\"}");
        RechargeOrderResp resp = client.rechargeOrder(req);
        TestCase.assertTrue(resp.isRepeat());
    }

    @Test
    public void rechargeProduct() throws Exception {
        RechargeProductResp resp = client.rechargeProduct();
        TestCase.assertTrue(resp.isSuccess());
        TestCase.assertTrue(resp.getProducts().size() > 0);
        TestCase.assertTrue(resp.getProducts().get(0).getProductId() > 0);
    }

    @Test
    public void rechargeQuery() throws Exception {
        String outTradeNo = "41858615686074369";
        RechargeQueryResp resp = client.rechargeQuery(outTradeNo);
        TestCase.assertEquals(RespCode.SUCCESS, resp.getCode());
        TestCase.assertTrue(resp.isFailed());
        TestCase.assertEquals(outTradeNo, resp.getOutTradeNo());
    }

    @Test
    public void NotifyTest() {
        Notify notify = new Notify(testMerchantId, testSecretKey);
        // ------------------------- test 签名错误 -------------------------
        TreeMap<String, String> postData = new TreeMap<>();
        String outTradeNo = "23121500060000001701";
        postData.put("merchantId", testMerchantId);
        postData.put("outTradeNo", outTradeNo);
        postData.put("status", "01");
        postData.put("rechargeAccount", "13499990003");
        postData.put("sign", "DF8A0AF7816538F55E221F9F52124FA3");
        try {
            notify.parseAndVerify(postData);
        } catch (OpenApiException e) {
            TestCase.assertEquals(new Integer(10001), e.getCode());
        } catch (Exception e) {
            TestCase.fail("发生异常：" + e.getMessage());
        }
        // ------------------------- test 签名通过 -------------------------
        postData.put("sign", "EF8A0AF7816538F55E221F9F52124FA3");
        try {
            NotifyOrderReq notifyOrderReq = notify.parseAndVerify(postData);
            TestCase.assertTrue(notifyOrderReq.isSuccess());
        } catch (Exception e) {
            TestCase.fail("发生异常：" + e.getMessage());
        }

        // ------------------------- 签名通过，且存在卡密 -------------------------
        postData = new TreeMap<>();
        outTradeNo = "RD6T19L9C1QZL9JDB";
        postData.put("merchantId", testMerchantId);
        postData.put("status", "01");
        postData.put("outTradeNo", outTradeNo);
        postData.put("cardCode", "CFntAGUhoWWQisGl8Tf9bA==");
        postData.put("sign", "00A47E7D95130ECAA1DD340AA2A52192");
        try {
            NotifyOrderReq notifyOrderReq = notify.parseAndVerify(postData);
            TestCase.assertTrue(notifyOrderReq.isSuccess());
            Map<String, String> cardMap = DecodeCard.decode2Map(notifyOrderReq.getCardCode(), testSecretKey);
            TestCase.assertTrue(cardMap.containsKey("6458631834234"));
            TestCase.assertEquals("", cardMap.get("6458631834234"));
        } catch (Exception e) {
            TestCase.fail("发生异常：" + e.getMessage());
        }
    }
}